#= require jquery
#= require jquery_ujs
#= require handlebars
#= require ember
#= require ember-data
#= require_self
#= require dumb_store

# for more details see: http://emberjs.com/guides/application/
window.DumbStore = Ember.Application.create()

